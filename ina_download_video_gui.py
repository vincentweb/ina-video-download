# -*- coding: utf-8 -*-

"""

All string input: s_unicode = s.decode("utf-8")  str --> unicode
All string output: s = s_unicode.encode("utf-8")  unicode --> str

"""

import ina_download_video
import os
import sys

from PyQt4 import QtCore, QtGui

class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent = None):
        QtGui.QMainWindow.__init__(self, parent)
        self.frame = QtGui.QFrame()        
        # Form layout, HBox
        self.fl = QtGui.QFormLayout()
                
        # Textbox label
        self.bhl = QtGui.QHBoxLayout()
        self.fl.addRow(QtGui.QLabel("URL INA : "), self.bhl)
        
        # INA URL textbox, on the same row of the label
        self.ina_url_textbox = QtGui.QLineEdit(self)
        self.ina_url_textbox.setMinimumWidth(300)
        self.ina_url_textbox.setText("ex : http://www.ina.fr/PackVOD/PACK289332104")
        self.bhl.addWidget(self.ina_url_textbox)

        # Download button, below the textbox
        self.download_button = QtGui.QPushButton('Ouvre et telecharge la video INA', self)
        self.download_button.clicked.connect(self.download_ina_video)
        self.fl.addRow(self.download_button)
        
        # Downloaded video destination folder, below the download button
        self.destination = os.getcwd()
        self.destination_label = QtGui.QLabel("Destination : %s" % self.destination)
        self.fl.addRow(self.destination_label)
        self.destination_button = QtGui.QPushButton('Changer la destination', self)
        self.destination_button.clicked.connect(self.change_destination_folder)
        self.fl.addRow(self.destination_button)
        
        
        # Render the frame     
        self.frame.setLayout(self.fl)
        self.setCentralWidget(self.frame)
        self.show()
        
    def change_destination_folder(self):
        self.destination = str(QtGui.QFileDialog.getExistingDirectory(self, "Select Directory").toUtf8()).decode('utf-8')
        self.destination_label.setText("Destination : %s" % self.destination)
        
        
    def download_ina_video(self):
        url = str(self.ina_url_textbox.displayText())
        destination = self.destination
        print ("Downloading", url)
        
        class DownloadWorker(QtCore.QObject):
            finished = QtCore.pyqtSignal()
        
            def __init__(self, *args, **kwargs):
                QtCore.QObject.__init__(self, *args, **kwargs)
        
            @QtCore.pyqtSlot()
            def process(self):
                ina_download_video.download_ina_video(url, destination)
                self.finished.emit()
                
        self.thread = QtCore.QThread(self)
        
        self.obj = DownloadWorker()  # no parent!
        
        self.obj.moveToThread(self.thread)
        self.obj.finished.connect(self.thread.quit)
        self.thread.started.connect(self.obj.process)
        self.thread.start()

        
if __name__ == "__main__":
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_X11InitThreads)
    app = QtGui.QApplication(sys.argv)
    window = MainWindow()
    app.exec_()
