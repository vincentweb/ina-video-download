# -*- coding: utf-8 -*-

import json
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import subprocess
import sys
import time

try:
    import credentials
except:
    print ("Please create a file credentials.py with the string variables 'login' and 'password'")
    sys.exit(-2)

#print 'RUN FIRST to avoid the invalid JSON error timestamp":190008,557947'
#print 'export LC_NUMERIC="en_US.UTF-8"'

def download_ina_video(url, destination = os.getcwd()):
    
    # Chrome driver is better because it waits the page is loaded before going further
    # With Firefox you have to add wait()
    driver = webdriver.Chrome()
    #driver = webdriver.Safari()
    #driver = webdriver.Firefox()
        
    #url = "http://www.ina.fr/video/CPF86604285/les-dix-petits-negres-d-agatha-christie-video.html"
    #url = "http://www.ina.fr/PackVOD/PACK289332104"
    #url = "http://www.ina.fr/audio/PHD88020441/permission-de-detente-audio.html"
    
    #url = "http://www.ina.fr/video/CAB8301572201/assassinat-kennedy-video.html"
    #url = "http://www.ina.fr/video/I00013182/la-premiere-marche-d-armstrong-video.html"
    #%%  Login
    driver.get(url)
    
    # FIXME test we are not already logged in
    print ("Auth")
    try:
        connexion_link = driver.find_element_by_id("headerLienConnexion")
        connexion_link.send_keys(Keys.RETURN)
        time.sleep(4)
    
        print ("Login")
        usename = driver.find_element_by_id("username")
        usename.clear()
        usename.send_keys(credentials.login)
        passwd = driver.find_element_by_id("txtPassword")
        passwd.clear()
        passwd.send_keys(credentials.password)
        passwd.send_keys(Keys.RETURN)
    except:
        print ("Already logged in?")
    
    # We should wait we are fully logged
    time.sleep(12)
    
    #%% Get and download the video
    print ("Searching video")
    title = None
    try:
        metadata = driver.find_element_by_css_selector("div.slideshow.slick-initialized.slick-slider > div > div > div.encours.slick-slide.slick-active > a")
        title = metadata.get_attribute("data-title")
        print ("Title; pack-method", title)
    except:
        try:
            metadata = driver.find_element_by_css_selector("#noticeFull > div > div.col-xs-8 > section > div.notice__headings > h2")
            title = metadata.text
            print ("Title; normal-h2-method", title)
        except:
            try:
                metadata = driver.find_element_by_css_selector("#main > div > div.container.space-pad-top.space-pad-bottom--sm > div:nth-child(1) > h1")
                title = metadata.text()
                print ("Title; normal-h1-method", title)
            except:
                pass
            
    title = title.replace("'", " ").replace('"', ' ')
    if title is not None:
        # "html body.connecte-abonne div#page div#page-content-position div#page-content div#columns.float-break div#main-position div#main.float-break div.overflow-fix div.container.space-pad-top.space-pad-bottom--sm div.row section#stackMedias.block-medias.space-marg-top--sm div.block-medias__show.premium div.ina-player.inaPlayerGlobal div.jwplayer.playlist-none.jw-user-inactive span.jwmain span.jwvideo video"
        video = driver.find_element_by_css_selector("#stackMedias div div div span span.jwvideo video")
        
        
        print ("Found",  title, video.get_attribute("src"))
        # FIXME: assume xterm is installed an the OS is Linux
        #cmd = 'xterm -e "avconv -i \"%s\" -vcodec copy -acodec copy  \"%s.mp4\" &"' % (video.get_attribute("src"), title)
        destination_file = os.path.join(destination, title+".mp4")
        # -hide_banner -loglevel panic -nostats
        #/home/vins/python/None.mp4
        cmd = 'avconv -loglevel panic  -i "%s" -vcodec copy -acodec copy  "%s"' % (video.get_attribute("src"), destination_file)
        print ("Running", cmd)
        process = subprocess.Popen(cmd.encode("utf-8"), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        out, err = process.communicate()
        print ("Output", out, err)
        
    driver.close()
    
if __name__ == "__main__":
    if not len(sys.argv) == 2:
        print ("Usage %s <http_ina_url_of_th_video>" % sys.argv[0])
        sys.exit(-1)
    download_ina_video(sys.argv[1])